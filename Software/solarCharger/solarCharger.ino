#include <avr/io.h>
#include <util/delay.h>

const int SW_PIN = 3;
const int LED_PIN = 2;
const int CURRENT_PIN = A0;
const int INPUT_VOLTAGE_PIN = A1;
const int OUTPUT_VOLTAGE_PIN = A2;
const unsigned DUTY_MAX = 63;
const unsigned FLASH_SPEED = 3000;
const unsigned ON_FLASH_TIME = 50;
const float V_STEP = 5.f / 1023.f;
const float IN_V_SCALE = 22.f / 1023.f;
const float OUT_V_SCALE = 40.f / 1023.f;


unsigned dutyCycle = 0;
unsigned long nextFlash = 0;
bool LEDState = 0;
int multiplier = 1;
float powerLast = 0.f;

void updateDutyCycle(unsigned cycle) {
  if (cycle > DUTY_MAX) {
    cycle = DUTY_MAX;
  }
  OCR2B = cycle;
}

void setPercentDutyCycle(float percent) {
  if (percent >= 100) {
    percent = 100.f;
  }
  auto p = percent / 100.f;
  updateDutyCycle(static_cast<unsigned>(p * DUTY_MAX));
}

void setup() {
  Serial.begin(115200);
  pinMode(SW_PIN, OUTPUT); // output pin for OCR2B, this is Arduino pin number
  pinMode(LED_PIN, OUTPUT);
  pinMode(CURRENT_PIN, INPUT);
  pinMode(INPUT_VOLTAGE_PIN, INPUT);
  pinMode(OUTPUT_VOLTAGE_PIN, INPUT);
  
  // In the next line of code, we:
  // 1. Set the compare output mode to clear OC2A and OC2B on compare match.
  //    To achieve this, we set bits COM2A1 and COM2B1 to high.
  // 2. Set the waveform generation mode to fast PWM (mode 3 in datasheet).
  //    To achieve this, we set bits WGM21 and WGM20 to high.
  TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);

  // In the next line of code, we:
  // 1. Set the waveform generation mode to fast PWM mode 7 —reset counter on
  //    OCR2A value instead of the default 255. To achieve this, we set bit
  //    WGM22 to high.
  // 2. Set the prescaler divisor to 1, so that our counter will be fed with
  //    the clock's full frequency (16MHz). To achieve this, we set CS20 to
  //    high (and keep CS21 and CS22 to low by not setting them).
  TCCR2B = _BV(WGM22) | _BV(CS20);

  // OCR2A holds the top value of our counter, so it acts as a divisor to the
  // clock. When our counter reaches this, it resets. Counting starts from 0.
  // Thus 63 equals to 64 divs.
  OCR2A = 63;
  // This is the duty cycle. Think of it as the last value of the counter our
  // output will remain high for. Can't be greater than OCR2A of course. A
  // value of 0 means a duty cycle of 1/64 in this case.
  OCR2B = 0;
  setPercentDutyCycle(0.f);
}

void loop() {
  if (millis() > nextFlash) {
    LEDState = !LEDState;
    if (LEDState) {
      nextFlash = millis() + ON_FLASH_TIME;
    } else {
      nextFlash = millis() + FLASH_SPEED;
    }
    digitalWrite(LED_PIN, LEDState);
  }

  // 50 is the conversion factor defined by the datasheet dependent on Rout of the current sensor IC
  auto current = analogRead(CURRENT_PIN) * V_STEP / 50.f;
  // 
  auto inV = analogRead(INPUT_VOLTAGE_PIN) * IN_V_SCALE;
  auto outV = analogRead(OUTPUT_VOLTAGE_PIN) * OUT_V_SCALE;
  auto power = current * inV;

  // If power starts dropping, reverse direction of pwm, Or if we are railed, and trying to go further, flip it just in case.
  if (power < powerLast || multiplier > 0 && dutyCycle == DUTY_MAX || multiplier < 0 && dutyCycle == 0) {
    multiplier = -1 * multiplier;
  }

  if (multiplier > 0 && dutyCycle < 63 || multiplier < 0 && dutyCycle > 0) {
    dutyCycle += multiplier;
  }

  updateDutyCycle(dutyCycle);
  
  powerLast = current * inV;

//  Serial.print("Current: ");
//  Serial.print(current);
//  Serial.print(" A\n");
//
//
//  Serial.print("IN Voltage: ");
//  Serial.print(inV);
//  Serial.print(" V\n");
//
//  Serial.print("OUT Voltage: ");
//  Serial.print(outV);
//  Serial.print(" V\n");
//
//  Serial.print("PWM Val: ");
//  Serial.print(dutyCycle);
//  Serial.print("\n");
//  delay(100);
}
